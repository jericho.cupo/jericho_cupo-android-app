package com.example.cuposcontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cuposcontacttracing.databinding.ActivityConfirmationBinding
import com.google.android.material.textfield.TextInputEditText

class Confirmation : AppCompatActivity() {

    private lateinit var binding: ActivityConfirmationBinding

    private var listTasks: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()


    }

    private fun init() {
        setUpRecyclerView()
        binding.confirmBtn.setOnClickListener {
            val intent = Intent(this,Success::class.java)
            startActivity(intent)
        }
    }

    private fun setUpRecyclerView()
    {
        val fullName = intent.extras?.getString("fullName")
        val phone = intent.extras?.getString("phone")
        val city = intent.extras?.getString("city")
        val ans1 = intent.extras?.getString("ans1")
        val ans2 = intent.extras?.getString("ans2")

        listTasks.add("Fullname: " + fullName.toString())
        listTasks.add("Contact Number: "+phone.toString())
        listTasks.add("City of Residence: "+city.toString())
        listTasks.add("First Question: " +ans1.toString())
        listTasks.add("Second Question: "+ans2.toString())

        binding.recyclerView.apply {
            adapter = RecyclerViewAdapter(list = listTasks)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

}