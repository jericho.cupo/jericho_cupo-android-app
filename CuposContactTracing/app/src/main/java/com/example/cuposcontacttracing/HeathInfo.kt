package com.example.cuposcontacttracing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.cuposcontacttracing.databinding.ActivityHeathInfoBinding

class HeathInfo : AppCompatActivity() {
    private lateinit var binding: ActivityHeathInfoBinding

    private var listTasks: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHeathInfoBinding . inflate (layoutInflater)
        setContentView(binding.root)
        init()
    }
    private fun init() {

        var btn = findViewById(R.id.next_2) as Button
        var firstQuestion = findViewById(R.id.firstQuestion) as EditText
        var secondQuestion = findViewById(R.id.secondQuestion) as EditText



        btn.setOnClickListener {
            val fullName = intent.extras?.getString("fullName")
            val phone = intent.extras?.getString("phone")
            val city = intent.extras?.getString("city")

            val firstQues: String = firstQuestion.text.toString()
            val secondQues: String = secondQuestion.text.toString();


            if (firstQues.trim().isNotEmpty() && secondQues.trim().isNotEmpty()) {
                val intent = Intent(this, Confirmation::class.java)
                intent.putExtra("fullName",fullName.toString())
                intent.putExtra("phone",phone.toString())
                intent.putExtra("city",city.toString())
                intent.putExtra("ans1",binding.firstQuestion.text.toString())
                intent.putExtra("ans2",binding.secondQuestion.text.toString())
                startActivity(intent)
            } else {
                Toast.makeText(
                    this@HeathInfo,
                    "Please complete all the necessary fields.",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
    }



}