package com.example.cuposcontacttracing

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cuposcontacttracing.databinding.ItemListBinding

class RecyclerViewAdapter (
    private val list: List<String>
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textViewItemTask.text = list[position]
    }

    override fun getItemCount() = list.size

    class ViewHolder(val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root)
}