package com.example.cuposcontacttracing

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.content.Intent
import android.widget.EditText
import com.example.cuposcontacttracing.databinding.ActivityMainBinding



class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

       private fun init(){

           var btn = findViewById(R.id.next_1) as Button
           var fullName = findViewById(R.id.fullName) as EditText
           var contactNumber = findViewById(R.id.contactNumber) as EditText
           var cityOfResidence = findViewById(R.id.cityOfResidence) as EditText

           btn.setOnClickListener {
               val fName: String = fullName.text.toString()
               val cNumber:String = contactNumber.text.toString();
               val cResidence:String = cityOfResidence.text.toString()

               if(fName.trim().isNotEmpty() && cNumber.trim().isNotEmpty() && cResidence.trim().isNotEmpty()){



                   val intent = Intent(this, HeathInfo::class.java)
                   intent.putExtra("fullName",binding.fullName.text.toString())
                   intent.putExtra("phone",binding.contactNumber.text.toString())
                   intent.putExtra("city",binding.cityOfResidence.text.toString())
                   startActivity(intent)
               }else{
                   Toast.makeText(this@MainActivity, "Please complete all the necessary fields.", Toast.LENGTH_SHORT).show()

               }
           }
       }

}